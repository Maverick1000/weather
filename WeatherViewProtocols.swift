//
//  WeatherViewProtocols.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//
import UIKit

// VIPER Interface for manipulating the navigation of the view
protocol WeatherNavigationInterface: class {
        //func nagvigateToCityWeatherDetials(_ detials: WeatherResponse)

}

// VIPER Interface for communication from Presenter -> View
protocol WeatherPresenterToViewInterface: class {
        func showAlertView(alertView view:UIAlertController)
}

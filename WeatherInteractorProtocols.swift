//
//  WeatherInteractorProtocols.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

// VIPER Interface for communication from Presenter to Interactor
protocol WeatherPresenterToInteractorInterface: class {
        func fectchWeahterDetialsForCity(withCity city:String, withLocation:String)
}

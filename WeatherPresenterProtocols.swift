//
//  WeatherPresenterProtocols.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//
import UIKit

// VIPER Interface for communication from Interactor -> Presenter
protocol WeatherInteractorToPresenterInterface: class {
        func fetchSuccessFulWeatherDetial(_ detials: WeatherResponse)
        func fetchErrorWeatherDetial(_ error: RESTAPIErrorResponse)
}

// VIPER Interface for communication from View -> Presenter
protocol WeatherViewToPresenterInterface: class {
        func userRequestSearch(withCity city:String, withLocation location:String)
}

// VIPER Interface for communication from Wireframe -> Presenter
protocol WeatherWireframeToPresenterInterface: class {
}

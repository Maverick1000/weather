//
//  Weather.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class Weather: NSObject {
	var id: Int = 0
	var main: String!
	var WeatherDescription: String!
	var icon: String!
        
        class func dictionaryMapping()->[String:String]{
                return ["id":"id","main":"main","description":"WeatherDescription","icon":"icon"]
        }
}

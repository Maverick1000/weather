//
//  WeatherCityDetialsView.swift
//  Weather
//
//  Created by Paul Walker on 10/19/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import UIKit

class WeatherCityDetialsView : UIViewController, WeatherCityDetialsPresenterToViewInterface,
                                                UITableViewDelegate,UITableViewDataSource {
        
        // MARK: - VIPER Stack
        //var presenter : WeatherCityDetialsViewToPresenterInterface!
        
        // MARK: - Instance Variables
        var forecastLists = [ForecastList]()
        
        // MARK: - Outlets
        @IBOutlet weak var cityName: UILabel!
        @IBOutlet weak var weatherImage: UIImageView!
        @IBOutlet weak var weather: UILabel!
        @IBOutlet weak var temperature: UILabel!
        @IBOutlet var forecastTableView: UITableView!

        // MARK: - Operational
        override func viewDidLoad() {
                forecastTableView.rowHeight = UITableViewAutomaticDimension
                forecastTableView.estimatedRowHeight = 75
        }
        override func viewWillDisappear(_ animated: Bool) {
                super.viewWillDisappear(animated)
                forecastLists.removeAll()
        }

        func numberOfSections(in tableView: UITableView) -> Int {
                return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return forecastLists.count
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as! ForecastTableViewCell
                cell.setForecastCell(properties: forecastLists[indexPath.row])
                return cell
        }

        
        // MARK: - Presenter to View Interface
        
        func showCityName(cityName: String) {
                self.cityName.text = cityName
        }
        
        func showForecast(forecastList:[ForecastList]){
                self.forecastLists.append(contentsOf: forecastList)
                forecastTableView.reloadData()
        }
        
        func showWeatherType(_ weather: String) {
                self.weather.text = weather
        }
        func showTemperature(_ temperature:String){
                self.temperature.text = temperature
        }
        func showWeatherImage(_ iconURLPath: URL) {
                weatherImage.setImageWith(iconURLPath)
        }
}

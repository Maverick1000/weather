//
//  Coord.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation

class Coord:NSObject {
        var lon:Double = 0.0
        var lat:Double = 0.0
        
        class func mapping() -> [String:String] {
                return ["lon":"lon","lat":"lat"]
        }
}

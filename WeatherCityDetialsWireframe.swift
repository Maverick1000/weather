//
//  WeatherCityDetialsWireframe.swift
//  Weather
//
//  Created by Paul Walker on 10/19/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import UIKit

class WeatherCityDetialsWireframe : WeatherCityDetialsPresenterToWireframeInterface {
        
        // MARK: - VIPER Stack
        lazy var moduleInteractor = WeatherCityDetialsInteractor()
        // Uncomment the comment lines and delete the moduleView line default code to use a navigationController from storyboard
        /*
        lazy var moduleNavigationController: UINavigationController = {
                let sb = WeatherCityDetials.storyboard()
                let v = sb.instantiateViewControllerWithIdentifier(kWeatherCityDetialsNavigationControllerIdentifier) as! UINavigationController
                return v
        }()
        */
        lazy var modulePresenter = WeatherCityDetialsPresenter()
        /*
        lazy var moduleView: WeatherCityDetialsView = {
                return self.moduleNavigationController.viewControllers[0] as! WeatherCityDetialsView
        }()
        */
        
        lazy var moduleView: WeatherCityDetialsView = {
                let sb = WeatherCityDetialsWireframe.storyboard()
                let vc = sb.instantiateViewController(withIdentifier:kWeatherCityDetialsViewIdentifier) as! WeatherCityDetialsView
                return vc
        }()
        lazy var presenter : WeatherCityDetialsWireframeToPresenterInterface = self.modulePresenter

        // MARK: - Instance Variables
        var delegate: WeatherCityDetialsDelegate?
        
        // MARK: - Initialization
        init() {
                
                let i = moduleInteractor
                let p = modulePresenter
                let v = moduleView
                
                i.presenter = p
                
                p.interactor = i
                p.view = v
                p.wireframe = self
                
                //v.presenter = p
                
                presenter = p
        }

	class func storyboard() -> UIStoryboard {
                return UIStoryboard(name: kWeatherCityDetialsStoryboardIdentifier, bundle: Bundle(for: WeatherCityDetialsWireframe.self))
	}
        
        // MARK: - Operational
        
        // MARK: - Module Interface
        
        // MARK: - Wireframe Interface
        func presentWeatherDeitalsForCity(_ navigationController: UINavigationController, withDetilaResponse response: WeatherResponse) {
                navigationController.pushViewController(moduleView, animated: true) { () in
                        self.modulePresenter.presentingWeatherDetials(response)
                }
        }
        
}

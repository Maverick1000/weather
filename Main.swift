//
//  Main.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class Main:NSObject{
	var temp: Double = 0.0
	var pressure: Double = 0.0
	var humidity: Double = 0.0
	var temp_min: Double = 0.0
	var temp_max: Double = 0.0
	var sea_level: Double = 0.0
	var grnd_level: Double = 0.0

        
	class func mapping() -> [String: String] {
		return ["temp": "temp", "pressure": "pressure",
			"humidity": "humidity", "temp_min": "temp_min",
			"temp_max": "temp_max", "sea_level": "sea_level",
			"grnd_level": "grnd_level"]
	}
}

//
//  CityListService.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

protocol CityListServiceProtorcal {
	func requestCityLists() -> NSArray?
}
class CityListService: CityListServiceProtorcal
{
	func requestCityLists() -> NSArray? {
		var response: NSArray?

		if let file = Bundle.main.path(forResource: "city.list.us", ofType: "json", inDirectory: "") {
			do {
				let jsonString = try String(contentsOfFile: file)
				let data = jsonString.data(using: String.Encoding.utf8)
				let parserData = try RKMIMETypeSerialization.object(from: data, mimeType: "application/json")

				let mapper = RKMapperOperation(representation: parserData, mappingsDictionary: ["": CityModel.repsonseRKMapping()])
				try mapper?.execute()
				response = mapper?.mappingResult.array() as NSArray?
			}
			catch {
				print(error)
			}
		}

		return response
        }
}

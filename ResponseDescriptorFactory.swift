//
//  ResponseDescriptorFactory.swift
//  Weather
//
//  Created by Paul Walker on 10/20/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import Foundation
import RestKit

class ResponseDescriptorFactory: NSObject {
        class func responseDescriptorForSuccess(mapping:RKMapping,withMethod method:RKRequestMethod,
                                                           withPathPattern  pathPattern:String?, withKeyPath path:String? ) ->RKResponseDescriptor{
                
                let statusCodes = RKStatusCodeIndexSetForClass(.successful)
                let responseDescriptor = RKResponseDescriptor(mapping: mapping, method: .GET,
                                                                          pathPattern: nil, keyPath: nil, statusCodes: statusCodes)

                return responseDescriptor!
        }
        
        class func responseDescriptorForClientErrorWithMapping(mapping:RKObjectMapping?) ->RKResponseDescriptor{
                var errorMapping = mapping
                
                if errorMapping == nil{
                        errorMapping = RKObjectMapping(for: RESTAPIErrorResponse().classForCoder)
                        errorMapping!.addPropertyMapping(RKAttributeMapping(fromKeyPath: "message", toKeyPath: "errorMessage"))
                }
                
                let errorDescriptor = RKResponseDescriptor(mapping: errorMapping, method: .any, pathPattern: nil, keyPath: nil,
                                                                                statusCodes: RKStatusCodeIndexSetForClass(.clientError))
                
                return errorDescriptor!
        }
        
        class func responseDescriptorForInformationalErrorWithMapping(mapping:RKObjectMapping?) ->RKResponseDescriptor{
                var errorMapping = mapping
                
                if errorMapping == nil{
                        errorMapping = RKObjectMapping(for: RESTAPIErrorResponse().classForCoder)
                        errorMapping!.addPropertyMapping(RKAttributeMapping(fromKeyPath: "message", toKeyPath: "errorMessage"))
                }
                
                let errorDescriptor = RKResponseDescriptor(mapping: errorMapping, method: .any, pathPattern: nil, keyPath: nil,
                                                           statusCodes: RKStatusCodeIndexSetForClass(.informational))
                
                return errorDescriptor!
        }
        
        class func responseDescriptorForClassRedirectionErrorWithMapping(mapping:RKObjectMapping?) ->RKResponseDescriptor{
                var errorMapping = mapping
                
                if errorMapping == nil{
                        errorMapping = RKObjectMapping(for: RESTAPIErrorResponse().classForCoder)
                        errorMapping!.addPropertyMapping(RKAttributeMapping(fromKeyPath: "message", toKeyPath: "errorMessage"))
                }
                
                let errorDescriptor = RKResponseDescriptor(mapping: errorMapping, method: .any, pathPattern: nil, keyPath: nil,
                                                           statusCodes: RKStatusCodeIndexSetForClass(.redirection))
                
                return errorDescriptor!

        }
        
        class func responseDescriptorForClassServerErrorWithMapping(mapping:RKObjectMapping?) ->RKResponseDescriptor{
                var errorMapping:RKObjectMapping? = mapping
                
                if errorMapping == nil{
                        errorMapping = RKObjectMapping(for: RESTAPIErrorResponse().classForCoder)
                        errorMapping!.addPropertyMapping(RKAttributeMapping(fromKeyPath: "message", toKeyPath: "errorMessage"))
                }
                
                let errorDescriptor = RKResponseDescriptor(mapping: errorMapping, method: .any, pathPattern: nil, keyPath: nil,
                                                           statusCodes: RKStatusCodeIndexSetForClass(.serverError))
                
                return errorDescriptor!

        }
        
        
        
        class func responseDescriptorForClientError() ->RKResponseDescriptor{
                return responseDescriptorForClientErrorWithMapping(mapping: nil)
        }
        
        class func responseDescriptorForInformationalError() ->RKResponseDescriptor{
                return responseDescriptorForInformationalErrorWithMapping(mapping: nil)
        }
        
        class func responseDescriptorForClassRedirectionError() ->RKResponseDescriptor{
                return responseDescriptorForClassRedirectionErrorWithMapping(mapping: nil)
        }
        
        class func responseDescriptorForClassServerError() ->RKResponseDescriptor{
                return responseDescriptorForClassServerErrorWithMapping(mapping: nil)
        }
        
        class func errorDescriptorArray() ->[RKResponseDescriptor]{
                let clientErrorDescriptor = responseDescriptorForClientError()
                let informationalErrorDescriptor = responseDescriptorForInformationalError()
                let redirectionErrorDescriptor = responseDescriptorForClassRedirectionError()
                let classServerErrorDescriptor = responseDescriptorForClassServerError()
                
                return [clientErrorDescriptor,informationalErrorDescriptor,redirectionErrorDescriptor,classServerErrorDescriptor]
        }
        
}

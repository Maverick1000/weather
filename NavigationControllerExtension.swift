//
//  NavigationControllerExtension.swift
//  Weather
//
//  Created by Paul Walker on 6/1/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit.UINavigationController

public typealias VoidBlock = ((Void) -> Void)

public extension UINavigationController
{
	public func pushViewController(_ viewController: UIViewController, animated: Bool, completion: @escaping VoidBlock) {
		CATransaction.begin()
		CATransaction.setCompletionBlock(completion)
		self.pushViewController(viewController, animated: animated)
		CATransaction.commit()
	}
}

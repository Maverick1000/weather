//
//  WeatherCityDetialsProtocols.swift
//  Weather
//
//  Created by Paul Walker on 10/19/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import UIKit

let kWeatherCityDetialsNavigationControllerIdentifier = "WeatherCityDetialsNavigationController"
let kWeatherCityDetialsStoryboardIdentifier = "Weather"
let kWeatherCityDetialsViewIdentifier = "WeatherCityDetialsView"

// Messages outgoing from the module
protocol WeatherCityDetialsDelegate : class {
        
}

// VIPER Interface for communication from Wireframe -> Presenter
protocol WeatherCityDetialsWireframeToPresenterInterface : class {
        func presentingWeatherDetials(_ detials: WeatherResponse)
        
}

// VIPER Interface for communication from Presenter -> Wireframe
protocol WeatherCityDetialsPresenterToWireframeInterface : class {
         func presentWeatherDeitalsForCity(_ navigationController: UINavigationController, withDetilaResponse response: WeatherResponse)
}

// VIPER Interface for communication from Interactor -> Presenter
protocol WeatherCityDetialsInteractorToPresenterInterface : class {
        func fetchSuccessFulWeatherForecast(_ response:ForecastResponse)
        func fetchErrorWeatherForecast(_ error:RESTAPIErrorResponse)
}

// VIPER Interface for communication from Presenter -> Interactor
protocol WeatherCityDetialsPresenterToInteractorInterface : class {
        func fectchDailyForecastWeather(id:Int, count:Int)
}

// VIPER Interface for communication from Presenter -> View
protocol WeatherCityDetialsPresenterToViewInterface : class {
        func showCityName(cityName: String)
        func showWeatherImage(_ iconURLPath: URL)
        func showWeatherType(_ weather: String)
        func showTemperature(_ temperature: String)
        func showForecast(forecastList:[ForecastList])
}

// VIPER Interface for communication from View -> Presenter
protocol WeatherCityDetialsViewToPresenterInterface : class {
        
}

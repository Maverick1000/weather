//
//  WeatherCityDetialsInteractor.swift
//  Weather
//
//  Created by Paul Walker on 10/19/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import Foundation

class WeatherCityDetialsInteractor:WeatherCityDetialsPresenterToInteractorInterface {
        
        // MARK: - VIPER Stack
        var presenter : WeatherCityDetialsInteractorToPresenterInterface!
        
        // MARK: - Instance Variables
        lazy var forecastWeatherService = ForecastWeatherService()
        
        // MARK: - Operational
        
        // MARK: - Presenter To Interactor Interface
        func fectchDailyForecastWeather(id:Int, count:Int) {
                
                forecastWeatherService.requestDailyForecastWeather(parameters:
                        ["APPID": kOPEN_WEAHTER_APIKEY, "id":id,"cnt":count, "units": "metric"], success: { (response:ForecastResponse) in
                        self.presenter.fetchSuccessFulWeatherForecast(response)
                }) { (error:RESTAPIErrorResponse?) in
                        self.presenter.fetchErrorWeatherForecast(error!)
                }
        }
        
}

//
//  Wind.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class Wind:NSObject{
	var speed: Double = 0.0
	var deg: Double = 0.0
        
        class func mapping() -> [String: String] {
                return ["speed": "speed", "deg": "deg"]
        }

}

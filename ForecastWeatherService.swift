//
//  ForecastWeatherService.swift
//  Weather
//
//  Created by Paul Walker on 11/2/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import UIKit
import RestKit

protocol ForecastWeatherServiceProtocol {
        func requestDailyForecastWeather(parameters: [AnyHashable : Any],
                                         success: @escaping (_ response:ForecastResponse) ->(),
                                         failure: @escaping (_ response:RESTAPIErrorResponse?) -> ())
}

class ForecastWeatherService: NSObject,ForecastWeatherServiceProtocol {
        
        func requestDailyForecastWeather(parameters: [AnyHashable : Any],
                                         success: @escaping (ForecastResponse) -> (),
                                         failure: @escaping (RESTAPIErrorResponse?) -> ()) {
                
                
                let path = "/data/2.5/forecast/daily?"
                
                RestkitNetwortManager.retrieveObjectUsingGet(path, parameters: parameters ,
                                                             responseMapping: ForecastResponse.requestRKMapping(),
                                                             success: { (mappingResponse:RKMappingResult) in
                                                                success(mappingResponse.firstObject as! ForecastResponse)
                }) { (error:RESTAPIErrorResponse) in
                        failure(error)
                }
                
        }
}

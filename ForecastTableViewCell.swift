//
//  ForecastTableViewCell.swift
//  Weather
//
//  Created by Paul Walker on 11/3/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import UIKit

class ForecastTableViewCell: UITableViewCell{
        @IBOutlet var displayDate: UILabel!
        @IBOutlet var todayLabel: UILabel!
        @IBOutlet var windSpeed: UILabel!
        
        func setForecastCell(properties:ForecastList){
                displayDate.text = properties.dt.CustomDateFromString(dateFormat:"EE dd MMM")
                if properties.dt.isDateTodday(){
                         todayLabel.text = "Today"
                }else{
                        todayLabel.isHidden = true
                }
                windSpeed.text = properties.speed.format(".2") + " m/s"
        }
}

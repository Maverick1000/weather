//
// Sys.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class Sys:NSObject{
        var message: Double = 0.0
	var country: String!
	var sunrise: Date!
	var sunset: Date!
        
        class func mapping() -> [String: String] {
                return ["message": "message","country": "country","sunrise": "sunrise","sunset": "sunset"]
        }
}

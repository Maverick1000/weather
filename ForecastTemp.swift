//
//  Temp.swift
//  Weather
//
//  Created by Paul Walker on 10/31/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import Foundation
import RestKit

class ForecastTemp:NSObject{
        var day:Double = 0.0
        var min:Double = 0.0
        var night:Double = 0.0
        var eve:Double = 0.0
        var morn:Double = 0.0
        
        class func dictionaryMapping()->[String:String]{
                return ["day":"day","min":"min","night":"night","eve":"eve","morn":"morn"]
        }
        class func arrayMapping()->[String]{
                return ["day","min","night","eve","morn"]
        }
        
        class func requestRKMapping()->RKMapping{
                let mapping = RKObjectMapping(with: ForecastTemp.classForCoder())
                mapping?.addAttributeMappings(from: ForecastTemp.arrayMapping())
                
                return mapping!
        }

}

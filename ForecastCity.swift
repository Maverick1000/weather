//
//  ForecastCity.swift
//  Weather
//
//  Created by Paul Walker on 10/31/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import Foundation
import RestKit

class ForecastCity:NSObject{
        var id:Int = 0
        var name:String!
        var coord:Coord!
        var country:String!
        var population:Int = 0
        
        class func dictionaryMapping()->[String:String]{
                return ["id":"id","name":"name","country":"country","population":"population"]
        }
        class func arrayMapping()->[String]{
                return ["id","name","country","population"]
        }
        
        class func requestRKMapping()->RKMapping{
                let mapping = RKObjectMapping(with: ForecastCity.classForCoder())
                mapping?.addAttributeMappings(from: ForecastCity.arrayMapping())
                
                let coordMapping = RKEntityMapping(with: Coord.classForCoder())
                coordMapping?.addAttributeMappings(from: Coord.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "coord", toKeyPath: "coord", with: coordMapping))

                return mapping!
        }
}

//
//  CityModel.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class CityModel:NSObject {
        var id:Int = 0
        var name:String!
        var country:String!
        var coord:Coord!
        
        class func dictionaryResponseMapping() -> [String:String] {
                return ["_id":"id","name":"name","country":"country"]
        }
        
        class func repsonseRKMapping()->RKMapping{
                let mapping = RKObjectMapping(with: CityModel.self)
                 mapping?.addAttributeMappings(from: CityModel.dictionaryResponseMapping())
                
                let coordMapping = RKEntityMapping(with: Coord.classForCoder())
                coordMapping?.addAttributeMappings(from: Coord.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "coord", toKeyPath: "coord", with: coordMapping))
                
                return mapping!
        }
}

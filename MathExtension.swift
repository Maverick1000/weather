//
//  MathExtension.swift
//  Weather
//
//  Created by Paul Walker on 5/2/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation

extension Double {
        func format(_ f: String) -> String {
                return String(format: "%\(f)f", self)
        }
}

extension Float {
        func format(_ f: String) -> String {
                return String(format: "%\(f)f", self)
        }
}

//
//  DateExtension.swift
//  Weather
//
//  Created by Paul Walker on 11/4/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import UIKit

public extension Date{
        func CustomDateFromString(dateFormat:String) -> String{
                let dateFromater = DateFormatter()
                dateFromater.dateFormat = dateFormat;
                
                return dateFromater.string(from: Date(timeIntervalSince1970: self.timeIntervalSince1970))
        }
        func isDateTodday() -> Bool{
                return NSCalendar.current.isDateInToday(self)
        }
}

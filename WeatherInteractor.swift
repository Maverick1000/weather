//
//  WeatherInteractor.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation

class WeatherInteractor:
WeatherPresenterToInteractorInterface
{
	// MARK: - VIPER Stack
	var presenter: WeatherInteractorToPresenterInterface!

	// MARK: - Instance Variables
	lazy var cityListService: CityListService = CityListService()
	lazy var currentWeatherService = CurrentWeatherService()
	// MARK: - Operational

	// MARK: - Interactor Input
        func fectchWeahterDetialsForCity(withCity city:String, withLocation location:String) {
                currentWeatherService.requestWeaherDataForCity(parameters:["q":"\(city)\(location)",
                        "APPID": kOPEN_WEAHTER_APIKEY, "units": "metric"], success: { (response:WeatherResponse) in
                                self.presenter.fetchSuccessFulWeatherDetial(response)
                }) { (error:RESTAPIErrorResponse?) in
                                self.presenter.fetchErrorWeatherDetial(error!)
                }
        }
}

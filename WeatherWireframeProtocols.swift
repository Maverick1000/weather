//
//  WeatherWireframeProtocols.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

// VIPER Module Constants
// Uncomment to utilize a navigation contoller from storyboard
let kWeatherNavigationControllerIdentifier = "WeatherNavigationController"
let kWeatherStoryboardIdentifier = "Weather"
let kWeatherViewIdentifier = "WeatherView"

// VIPER Interface to the Module
protocol WeatherDelegate : class {
        
}

// Interface Abstraction for working with the VIPER Module
protocol WeatherModuleInterface : class {
        
}

// VIPER Interface for communication from Presenter -> Wireframe
protocol WeatherPresesnterToWireframeInterface : class {
        func presentWeatherDetials(_ detials: WeatherResponse)
}

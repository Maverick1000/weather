//
//  WeatherCityDetialsPresenter.swift
//  Weather
//
//  Created by Paul Walker on 10/19/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import Foundation

class WeatherCityDetialsPresenter : WeatherCityDetialsInteractorToPresenterInterface, WeatherCityDetialsViewToPresenterInterface, WeatherCityDetialsWireframeToPresenterInterface {
     
        
        // MARK: - VIPER Stack
        var interactor : WeatherCityDetialsPresenterToInteractorInterface!
        var view : WeatherCityDetialsPresenterToViewInterface!
        var wireframe : WeatherCityDetialsPresenterToWireframeInterface!
        
        // MARK: - Instance Variables
        
        // MARK: - Operational
       


        // MARK: - Interactor to Presenter Interface
        internal func fetchSuccessFulWeatherForecast(_ response: ForecastResponse) {
                view.showForecast(forecastList: response.list)
        }
        
        internal func fetchErrorWeatherForecast(_ error: RESTAPIErrorResponse) {
                print("got here")
        }
        
              
        // MARK: - View to Presenter Interface
        
        // MARK: - Wireframe to Presenter Interface
        func presentingWeatherDetials(_ detials: WeatherResponse)
        {
                view.showCityName(cityName: detials.name)
                if let item = detials.weather.first {
                        view.showTemperature("\(detials.main.temp.format("0.1")) \u{2103}")
                        view.showWeatherImage(RestkitEnvironmentManager.requestImageURLWithPath(item.icon))
                        view.showWeatherType(item.WeatherDescription)
                }
                interactor.fectchDailyForecastWeather(id: detials.id, count: 13)
        }
}

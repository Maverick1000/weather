//
//  WeatherResponse.swift
//  Weather
//
//  Created by Paul Walker on 4/20/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

public class WeatherResponse: NSObject{

        var coord:Coord!
        var weather:[Weather]!
	var base: String!
        var main:Main!
        var visibility:Int = 0
        var wind:Wind!
        var rain:Rain!
        var clouds:Clouds!
	var dt: Date!
        var sys:Sys!
        var id: Int = 0
        var name: String!
        var cod: Int = 0
        
        class func dictionaryMapping()->[String:String]{
                return ["base":"base","dt":"dt","id":"id","visibility":"visibility","name":"name","cod":"cod"]
        }
        class func arrayMapping()->[String]{
                return ["base","dt","id","visibility","name","cod"]
        }
        
        class func requestRKMapping()->RKMapping{
                let mapping = RKObjectMapping(with: WeatherResponse.classForCoder())
                mapping?.addAttributeMappings(from: WeatherResponse.arrayMapping())
                
                let coordMapping = RKEntityMapping(with: Coord.classForCoder())
                coordMapping?.addAttributeMappings(from: Coord.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "coord", toKeyPath: "coord", with: coordMapping))
                
                let weatherMapping = RKEntityMapping(with: Weather.classForCoder())
                weatherMapping?.addAttributeMappings(from: Weather.dictionaryMapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "weather", toKeyPath: "weather", with: weatherMapping))
                
                let MainMapping = RKEntityMapping(with: Main.self)
                MainMapping?.addAttributeMappings(from: Main.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "main", toKeyPath: "main", with: MainMapping))
                
                let windMapping = RKEntityMapping(with: Wind.self)
                windMapping?.addAttributeMappings(from: Wind.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "wind", toKeyPath: "wind", with: windMapping))
                
                let rainMapping = RKEntityMapping(with:Rain.self)
                rainMapping?.addAttributeMappings(from: Rain.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "rain", toKeyPath: "rain", with: rainMapping))
                
                let cloudsMapping = RKEntityMapping(with: Clouds.self)
                cloudsMapping?.addAttributeMappings(from: Clouds.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "clouds", toKeyPath: "clouds", with: cloudsMapping))
                
                let sysMapping = RKEntityMapping(with: Sys.self)
                sysMapping?.addAttributeMappings(from: Sys.mapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "sys", toKeyPath: "sys", with: sysMapping))
        
                return mapping!
        }
}

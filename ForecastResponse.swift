//
//  ForecastResponse.swift
//  Weather
//
//  Created by Paul Walker on 10/31/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import Foundation
import RestKit

class ForecastResponse:NSObject{
        
        var city:ForecastCity!
        var cod:Int = 0
        var message:Double = 0.0
        var cnt:Int = 0
        var list:[ForecastList]!
        
        class func dictionaryMapping()->[String:String]{
                return ["cod":"cod","cnt":"cnt","message":"message"]
        }
        class func arrayMapping()->[String]{
                return ["cod","cnt","message"]
        }
        
        static func requestRKMapping()->RKMapping{
                let mapping = RKObjectMapping(with: ForecastResponse.classForCoder())
                mapping?.addAttributeMappings(from: ForecastResponse.arrayMapping())
                
                let forecastCityMapping = ForecastCity.requestRKMapping()
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "city", toKeyPath: "city", with: forecastCityMapping))

                let forecastListMapping = ForecastList.requestRKMapping()
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "list", toKeyPath: "list", with: forecastListMapping))
                
                return mapping!
        }

}

//
//  CurrentWeatherService.swift
//  Weather
//
//  Created by Paul Walker on 5/17/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit
import RestKit

typealias RestKitErrorHandler = (_ errorResponse:RESTAPIErrorResponse?) -> ()

protocol CurrentWeatherServiceProtocol {
        func requestWeaherDataForCity(parameters: [AnyHashable : Any],
                                      success: @escaping (_ response:WeatherResponse) ->(),
                                      failure: @escaping (_ response:RESTAPIErrorResponse?) -> ())
}

class CurrentWeatherService:CurrentWeatherServiceProtocol {
        
        func requestWeaherDataForCity(parameters: [AnyHashable : Any],
                                      success: @escaping (WeatherResponse) -> (),
                                      failure: @escaping (RESTAPIErrorResponse?) -> ()) {
                
                let path = "/data/2.5/weather?"
                
                RestkitNetwortManager.retrieveObjectUsingGet(path, parameters: parameters,
                                                             responseMapping:WeatherResponse.requestRKMapping(),
                                                             success: { (mappingResponse:RKMappingResult?) in
                                                                success(mappingResponse?.firstObject as! WeatherResponse)
                }) { (errorResponse:RESTAPIErrorResponse?) in
                        failure(errorResponse)
                }
                
        }
}

//
//  ForecastList.swift
//  Weather
//
//  Created by Paul Walker on 10/31/16.
//  Copyright © 2016 Maverick. All rights reserved.
//

import Foundation
import RestKit

class ForecastList:NSObject{
        
        var dt:Date!
        var temp:[ForecastTemp]!
        var pressure:Double = 0.0
        var humidity:Int = 0
        var weather:[Weather]!
        var speed:Double = 0.0
        var deg:Double = 0.0
        var clouds:Int = 0
        
        class func dictionaryMapping()->[String:String]{
                return ["dt":"dt","pressure":"pressure","humidity":"humidity","speed":"speed","deg":"deg","clouds":"clouds"]
        }
        class func arrayMapping()->[String]{
                return ["dt","pressure","humidity","speed","deg","clouds"]
        }
        
        class func requestRKMapping()->RKMapping{
                let mapping = RKObjectMapping(with: ForecastList.classForCoder())
                mapping?.addAttributeMappings(from: ForecastList.arrayMapping())
                
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "temp", toKeyPath: "temp", with: ForecastTemp.requestRKMapping()))

                let weatherWeatherMapping = RKEntityMapping(with: Weather.classForCoder())
                weatherWeatherMapping?.addAttributeMappings(from: Weather.dictionaryMapping())
                mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "weather", toKeyPath: "weather", with: weatherWeatherMapping))
                
                return mapping!
        }

}

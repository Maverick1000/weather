//
//  WeatherCityCell.swift
//  Weather
//
//  Created by Paul Walker on 5/2/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation

class WeatherCityCell: UITableViewCell {
	@IBOutlet weak var cityInfo: UILabel!
	@IBOutlet weak var cityGeoLocation: UILabel!
}
//
//  RestkitEnvironmentManager.swift
//  Weather
//
//  Created by Paul Walker on 5/3/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation

class RestkitEnvironmentManager {
	var baseURL: URL!
        var request: NSMutableURLRequest!

	init() {
		baseURL = URL(string: "http://api.openweathermap.org")
                request = NSMutableURLRequest(url: baseURL, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 120)
                request.addValue("application/json;charset=UTF-8", forHTTPHeaderField:"content-Type")
	}
        
        static func requestImageURLWithPath(_ path:String) -> URL {
                return URL(string: "http://api.openweathermap.org/img/w/\(path).png")!
        }
}

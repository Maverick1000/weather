//
//  RestkitNetwortManager.swift
//  Weather
//
//  Created by Paul Walker on 5/3/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import Foundation
import RestKit

class RestkitNetwortManager {
	var objectManager: RKObjectManager?
        var restkitEnvironmentManager: RestkitEnvironmentManager!

        var errorBlock:((_ result:RESTAPIErrorResponse) ->())?
        var getObjectRequestCompletionBlock:((_ result:RKMappingResult) ->())?

	init() {
		restkitEnvironmentManager = RestkitEnvironmentManager()
		objectManager = RKObjectManager(baseURL: restkitEnvironmentManager?.baseURL as URL!)
                objectManager?.requestSerializationMIMEType = RKMIMETypeJSON
        }

        func start(){
                //operation?.start()
        }
        
        class func retrieveObjectUsingGet(_ path: String, parameters: [AnyHashable: Any], responseMapping:RKMapping,
                                          success: @escaping (_ result: RKMappingResult) -> (), failure: @escaping (_ error: RESTAPIErrorResponse) -> ()){
                
                let successDescriptor = ResponseDescriptorFactory.responseDescriptorForSuccess(mapping: responseMapping,
                                                                        withMethod: .GET, withPathPattern: nil, withKeyPath: nil)
                
                var responseDescriptorArray = [successDescriptor]
                responseDescriptorArray.append(contentsOf: ResponseDescriptorFactory.errorDescriptorArray())
                
                return requestObjectAtPath(path, parameters: parameters, responseDescriptor: responseDescriptorArray,
                                           success: success, failure: failure)
        }
        
        private class func requestObjectAtPath(_ path: String, parameters: [AnyHashable: Any], responseDescriptor: [RKResponseDescriptor],
		success: @escaping (_ result: RKMappingResult) -> (), failure: @escaping (_ error: RESTAPIErrorResponse) -> ()){
			
                let manager = RestkitNetwortManager()
                
                manager.getObjectRequestCompletionBlock = success
                manager.errorBlock = failure
                
                
                manager.objectManager?.addResponseDescriptors(from: responseDescriptor)
                manager.getObjectWithPath(withPath: path, withParameters: parameters)
	}
        
        private func getObjectWithPath(withPath path:String, withParameters parameters: [AnyHashable: Any]){
                
                objectManager?.getObjectsAtPath(path, parameters: parameters, success: {
                                (operation:RKObjectRequestOperation?, mapping:RKMappingResult?) in
                                self.finalGetObjectRequestCompletionBlock(mapping: mapping!)
                        }, failure: {( operation:RKObjectRequestOperation?,error:Error?) in
                                self.errorBlock(operation: operation!, error: error!)
                })
        }
        
        
        func finalGetObjectRequestCompletionBlock(mapping:RKMappingResult ){
                getObjectRequestCompletionBlock!(mapping)
                getObjectRequestCompletionBlock = nil
        }
        
        func errorBlock(operation:RKObjectRequestOperation,error:Error){
                if(errorBlock != nil){
                        var errorResponse:RESTAPIErrorResponse!
                        let userInfoObjects = [(error as NSError).userInfo[RKObjectMapperErrorObjectsKey]]
                        
                        if userInfoObjects[0] is [RESTAPIErrorResponse]{
                                let errorObjects =  userInfoObjects[0] as! [RESTAPIErrorResponse]
                                errorResponse = errorObjects[0] as RESTAPIErrorResponse!
                        }
                        errorBlock!(errorResponse)
                        errorBlock = nil
                }
        }

}



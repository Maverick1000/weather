//
//  WeatherPresenter.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class WeatherPresenter:
WeatherInteractorToPresenterInterface,
WeatherViewToPresenterInterface,
WeatherWireframeToPresenterInterface
{
	// MARK: - VIPER Stack
        var interactor: WeatherPresenterToInteractorInterface!
        var view: WeatherPresenterToViewInterface!
        var wireframe: WeatherPresesnterToWireframeInterface!

	// MARK: - Instance Variables
	var results: NSArray?

	// MARK: - Operational

	// MARK: - Interactor Output
	func fetchSuccessFulWeatherDetial(_ detials: WeatherResponse) {
                wireframe.presentWeatherDetials(detials)
        }

	func fetchErrorWeatherDetial(_ error: RESTAPIErrorResponse) {
                let alertController =  UIAlertController(title: "Search Error", message: error.errorMessage, preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                
                alertController.addAction(OKAction)
                view.showAlertView(alertView: alertController)
	}

	// MARK: - Presenter Interface
        func userRequestSearch(withCity city:String, withLocation location:String){
                interactor.fectchWeahterDetialsForCity(withCity: city, withLocation: location)
        }

	// MARK: - Routing
}

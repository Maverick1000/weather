//
//  WeatherWireframe.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class WeatherWireframe:
WeatherModuleInterface,
WeatherPresesnterToWireframeInterface
{
	// MARK: - VIPER Stack
	lazy var moduleInteractor = WeatherInteractor()
	// Uncomment the comment lines and delete the moduleView line default code to use a navigationController from storyboard

	lazy var moduleNavigationController: UINavigationController = {
		let sb = WeatherWireframe.storyboard()
		let v = sb.instantiateViewController(withIdentifier: kWeatherNavigationControllerIdentifier) as! UINavigationController
		return v
	}()

	lazy var modulePresenter = WeatherPresenter()

	lazy var moduleView: WeatherView = {
		return self.moduleNavigationController.viewControllers[0] as! WeatherView
	}()

//	lazy var moduleView: WeatherView = {
//		let sb = WeatherWireframe.storyboard()
//		let vc = sb.instantiateViewControllerWithIdentifier(kWeatherViewIdentifier) as! WeatherView
//		return vc
//	}()
	lazy var presenter: WeatherWireframeToPresenterInterface = self.modulePresenter
	lazy var view: WeatherNavigationInterface = self.moduleView

        lazy var presenterDetials: WeatherCityDetialsWireframe = WeatherCityDetialsWireframe()
        
	// MARK: - Instance Variables
	var delegate: WeatherDelegate?

	// MARK: - Initialization
        init() {
        
		let i = moduleInteractor
		let p = modulePresenter
		let v = moduleView

		i.presenter = p

		p.interactor = i
		p.view = v
		p.wireframe = self

		v.presenter = p

		presenter = p
	}

	class func storyboard() -> UIStoryboard {
		return UIStoryboard(name: kWeatherStoryboardIdentifier, bundle: Bundle(for: WeatherWireframe.self))
	}

	// MARK: - Operational
        
	// MARK: - Module Interface
        
        // MARK: - Wireframe Interface
        func presentWeatherDetials(_ detials: WeatherResponse) {
                presenterDetials.presentWeatherDeitalsForCity(moduleView.navigationController!, withDetilaResponse: detials)
	}
}


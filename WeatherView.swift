//
//  WeatherView.swift
//  Weather
//
//  Created by Paul Walker on 4/29/16.
//  Copyright © 2016 Paul Walker. All rights reserved.
//

import UIKit

class WeatherView: UIViewController, WeatherNavigationInterface, WeatherPresenterToViewInterface
{
	// MARK: - VIPER Stack
        var presenter: WeatherViewToPresenterInterface!

	// MARK: - Instance Variables

	// MARK: - Outlets
        @IBOutlet var city: UITextField!
        @IBOutlet var country: UITextField!

	// MARK: - Operational
        override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(animated)
        }
        
        // MARK: - IBAction
         @IBAction func userTappedSearch(_ sender: AnyObject) {
                presenter.userRequestSearch(withCity: city.text!, withLocation: country.text!)
        }
        
        // MARK: - View Interface
        func showAlertView(alertView view:UIAlertController){
                present(view, animated: true, completion: nil)
        }

}
